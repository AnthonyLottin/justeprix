package JustePrix.src;// Fichier : Stat.java

import java.io.IOException;
import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    int nombreSecret = GenerateurNombreAleatoire.genererNombreAleatoire();
    int nombreProposer =-1;
    while (!VerificationNombre.estValide(nombreProposer, nombreSecret)) {
      nombreProposer = new Scanner(System.in).nextInt();
      if (VerificationNombre.estInferieur(nombreProposer, nombreSecret)) {

        System.out.println("Le nombre est inférieur.");

      } else if (VerificationNombre.estSuperieur(nombreSecret, nombreSecret)) {

        System.out.println("Le nombre est supérieur.");

      }
    }

    System.out.println("Vous avez trouvez le nombres...");

  }

  private static void reinitialiserPartie() {
    System.out.println("Voulez-vous jouer à nouveau ? (Oui/Non)");
    Scanner scanner = new Scanner(System.in);
    String reponse = scanner.nextLine().toLowerCase();
    if (reponse.equals("oui")) {
        main(null);
    } else {
        System.out.println("Merci d'avoir joué ! Au revoir.");
        System.exit(0);
    }
}

}