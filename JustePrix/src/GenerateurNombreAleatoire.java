package JustePrix.src;// Fichier : Stat.java
import java.util.Random;

public class GenerateurNombreAleatoire {

    private static int difficult = 3;
    public static int genererNombreAleatoire() {
        // Créez un objet Random pour générer des nombres aléatoires
        Random aleatoire = new Random();
        
        // Générez un nombre aléatoire
        int nombreAleatoire = aleatoire.nextInt(ajusterPlageDeNombres(difficult));
        
        return nombreAleatoire;
    }

    private static int ajusterPlageDeNombres(int niveauDifficulte) {
    switch (niveauDifficulte) {
        case 1:
            return 100; // Plage de nombres de 1 à 100
        case 2:
            return 200; // Plage de nombres de 1 à 200
        case 3:
            return 500; // Plage de nombres de 1 à 500
        default:
            return 100;
    }
}
}
