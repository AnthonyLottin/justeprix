package JustePrix.src;// Fichier : Stat.java

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public abstract class StaticStrings {

    public static String introduction = "C'est parti pour jouer. Je choisi un nombre... C'est bon, j'en ai un. À toi de deviner auquel je pense.";
    public static String plusPetit = "Il est plus petit.";
    public static String plusGrand = "Il est plus grand.";
    public static String trouve = "BRAVO ! Tu l'as trouvé !";
    public static String recommencer = "Tu veux faire une autre partie ?";


    public static List<String> rate = Arrays.asList(
            "Raté !",
            "Ce n'est pas le bon.",
            "Ce n'est pas celui auquel je pense."
    );
    public static List<String> gagne = Arrays.asList(
        "Bravo !",
        "Félicitation !",
        "Bien joué !"
    );



    public String rate(Boolean estPlusGrand) {
        String res = rate.get(new Random().nextInt(rate.size()));
        if (estPlusGrand) {
            res += this.plusGrand;
        } else {
            res += this.plusPetit;
        }
        return res;
    }
    public String gagne() {
        String res = gagne.get(new Random().nextInt(rate.size()));
        return res;
    }

}
