package JustePrix.src;// Fichier : Stat.java

public class Stastistique {
    private String nom;
    private int chiffreSecret;
    private int nombreDeCoups;

    public Stastistique(String nom, int chiffreSecret, int nombreDeCoups) {
        this.nom = nom;
        this.chiffreSecret = chiffreSecret;
        this.nombreDeCoups = nombreDeCoups;
    }

    public String getNom() {
        return nom;
    }

    public int getChiffreSecret() {
        return chiffreSecret;
    }

    public int getNombreDeCoups() {
        return nombreDeCoups;
    }
}