package JustePrix.src;// Fichier : Stat.java
class VerificationNombre{
    static public boolean estInferieur(int monNombre,int nombreSecret){
        return monNombre < nombreSecret; 
    }
    
    static public boolean estSuperieur(int monNombre, int nombreSecret){
        return monNombre > nombreSecret;
    }

    static public boolean estValide(int monNombre, int nombreSecret){
        return monNombre == nombreSecret;
    }
}