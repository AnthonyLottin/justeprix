
# JustePrix

Le projet Juste Prix est un jeu simple implémenté en Java qui met les compétences de l'utilisateur à l'épreuve en lui demandant de deviner un nombre mystère généré aléatoirement. Le joueur interagit avec le jeu via le terminal.


## Exemple d'utilisation du jeux 

![Logo](https://i.postimg.cc/h4XdNRQw/Capture-d-cran-2024-01-30-104523.png)


## Authors

- [@AnthonyLottin](https://gitlab.com/AnthonyLottin) Anthony Lottin
- [@Gwendal_Le_Tareau](https://gitlab.com/Gwendal_Le_Tareau) Gwendal Le Tareau
- [@anthony.bignon.contact](https://gitlab.com/anthony.bignon.contact) Anthony Bignon
- [@lecalvezmathieu22](https://gitlab.com/lecalvezmathieu22) Le Calvez Mathieu

